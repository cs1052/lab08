package cs105;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Fruit> fruits = new ArrayList<Fruit>();
        fruits.add(new Apple());
        fruits.add(new Banana());
        fruits.add(new Strawberry());
        fruits.add(new Blackberry());
        prepareFruits(fruits);
    }

    public static void prepareFruits(ArrayList<Fruit> fruits) {
        for (Fruit fruit : fruits) {
            if (fruit instanceof GroundFruit) {
                ((GroundFruit) fruit).pick();
            } else if (fruit instanceof TreeFruit) {
                ((TreeFruit) fruit).peel();
            }
        }
    }
}
