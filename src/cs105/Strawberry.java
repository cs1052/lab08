package cs105;

public class Strawberry extends GroundFruit {
    public Strawberry() {
        this.color = "red";
    }

    public String getVitamin() {
        return "B5 E";
    }

    public void pick() {
        System.out.println("Picking a strawberry.");
    }
}
