package cs105;

public class Banana extends TreeFruit {
    public Banana() {
        this.color = "yellow";
    }

    public String getVitamin() {
        return "C D";
    }

    public void peel() {
        System.out.println("Peeling a banana.");
    }
}
